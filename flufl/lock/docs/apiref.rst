=============
API Reference
=============

API reference for ``flufl.lock``:


.. autoclass:: flufl.lock.Lock
   :members:


Exceptions
==========

.. autoclass:: flufl.lock.AlreadyLockedError

.. autoclass:: flufl.lock.NotLockedError

.. autoclass:: flufl.lock.TimeOutError
